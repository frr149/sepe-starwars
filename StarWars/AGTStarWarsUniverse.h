//
//  AGTStarWarsUniverse.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 14/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import Foundation;

#import "AGTStarWarsCharacter.h"

@interface AGTStarWarsUniverse : NSObject

@property (nonatomic, readonly) NSUInteger imperialCount;
@property (nonatomic, readonly) NSUInteger rebelCount;

-(AGTStarWarsCharacter *) rebelCharacterAtIndex:(NSUInteger) index;
-(AGTStarWarsCharacter *) imperialCharacterAtIndex:(NSUInteger) index;


@end
