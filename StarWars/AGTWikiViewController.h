//
//  AGTWikiViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 13/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

#import "AGTStarWarsCharacter.h"

@interface AGTWikiViewController : UIViewController <UIWebViewDelegate>


@property (strong, nonatomic) AGTStarWarsCharacter *model;
@property (weak, nonatomic) IBOutlet UIWebView *browser;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

-(id) initWithModel:(AGTStarWarsCharacter*) model;

@end
