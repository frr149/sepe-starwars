//
//  AGTCharacterViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

#import "AGTStarWarsCharacter.h"
#import "CafPlayer.h"
#import "AGTStarWarsUniverseViewController.h"

@interface AGTCharacterViewController : UIViewController <UISplitViewControllerDelegate, AGTStarWarsUniverseViewControllerDelegate>


@property (strong, nonatomic) AGTStarWarsCharacter *model;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) CafPlayer *player;


-(id) initWithModel:(AGTStarWarsCharacter*)model;


-(IBAction)playSound:(id)sender;
-(IBAction)displayWikipedia:(id)sender;




@end

















