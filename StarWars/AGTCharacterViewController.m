//
//  AGTCharacterViewController.m
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 12/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCharacterViewController.h"
#import "CafPlayer.h"
#import "AGTWikiViewController.h"

@implementation AGTCharacterViewController


-(id) initWithModel:(AGTStarWarsCharacter*)model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
        self.title = model.alias;
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // sincronizo modelo -> vista
    [self syncWithModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Actions
-(IBAction)playSound:(id)sender{
    
    self.player = [CafPlayer cafPlayer];
    [self.player playSoundData:self.model.soundData];
}

-(IBAction)displayWikipedia:(id)sender{
    
    // Crear un WikiVC
    AGTWikiViewController *wikiVC = [[AGTWikiViewController alloc] initWithModel:self.model];
    
    // Pushearlo
    [self.navigationController pushViewController:wikiVC
                                         animated:YES];
    
    
}


#pragma mark - UISplitViewControllerDelegate
-(void)splitViewController:(UISplitViewController *)svc
   willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode{
    
    
    if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
        // Hay que poner el botón en mi barra de navegación
        self.navigationItem.rightBarButtonItem = svc.displayModeButtonItem;
        
        
    }else if (displayMode == UISplitViewControllerDisplayModeAllVisible){
        // Hay que quitar el botón de la barra de navegación (pareado incluido)        
        self.navigationItem.rightBarButtonItem = nil;
        
    }
}



#pragma mark - AGTStarWarsUniverseViewControllerDelegate

-(void) starWarsUniverseViewController:(AGTStarWarsUniverseViewController *)uVC
                    didSelectCharacter:(AGTStarWarsCharacter *)character{
    
    
    // me dicen que cambie mi modelo
    self.model = character;
    [self syncWithModel];
    
}

#pragma mark - Utils
-(void) syncWithModel{

    self.photoView.image = self.model.photo;
    self.title = self.model.alias;
    
}























@end
