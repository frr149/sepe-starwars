//
//  AGTStarWarsUniverseViewController.h
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 14/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define REBEL_SECTION 1
#define IMPERIAL_SECTION 0

#define CHARACTER_DID_CHANGE_NOTIFICATION_NAME @"CHARACTER"
#define CHARACTER_KEY @"CHARACTERKEY"


@import UIKit;

#import "AGTStarWarsUniverse.h"


@class AGTStarWarsUniverseViewController;

@protocol AGTStarWarsUniverseViewControllerDelegate <NSObject>

@optional
-(void)starWarsUniverseViewController:(AGTStarWarsUniverseViewController*) uVC
                   didSelectCharacter:(AGTStarWarsCharacter*) character;

-(void) starWarsUniverseViewController:(AGTStarWarsUniverseViewController*) uVC
                   willSelectCharacter:(AGTStarWarsCharacter*) character;

@end



@interface AGTStarWarsUniverseViewController : UITableViewController<AGTStarWarsUniverseViewControllerDelegate>

@property (weak,nonatomic) id<AGTStarWarsUniverseViewControllerDelegate> delegate;

-(id) initWithModel:(AGTStarWarsUniverse *) model
              style:(UITableViewStyle) style;

@end



